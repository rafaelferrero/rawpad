unit sysPrincipal;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, PrintersDlgs, Forms, Controls, Graphics, Dialogs,
  Menus, StdCtrls, ComCtrls, ExtCtrls, IniFiles, Printers
  {$ifdef windows},windows,shellapi{$endif}{$ifdef linux},LConvEncoding, PrintersDlgs{$endif};

type

  { TfrmPrincipal }

  TfrmPrincipal = class(TForm)
    FontDialog1: TFontDialog;
    Image1: TImage;
    Label1: TLabel;
    MI_Nuevo: TMenuItem;
    M_Noticia: TMemo;
    MenuItem1: TMenuItem;
    MI_Configuracion: TMenuItem;
    MI_Guardar: TMenuItem;
    MI_GuardarComo: TMenuItem;
    MI_Imprimir: TMenuItem;
    MI_Abrir: TMenuItem;
    MI_Archivo: TMenuItem;
    MM_Principal: TMainMenu;
    OpenDialog1: TOpenDialog;
    PrintDialog1: TPrintDialog;
    SaveDialog1: TSaveDialog;
    StatusBar1: TStatusBar;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure MI_AbrirClick(Sender: TObject);
    procedure MI_GuardarClick(Sender: TObject);
    procedure MI_GuardarComoClick(Sender: TObject);
    procedure MI_ImprimirClick(Sender: TObject);
    procedure MI_NuevoClick(Sender: TObject);
    procedure M_NoticiaChange(Sender: TObject);
    function StyleToStr(fuente:TFont):string;
    function StrToStyle(cadena:String):TFontStyles;
    procedure CargarIniFile;
    procedure buscarFuente;
    procedure MI_ConfiguracionClick(Sender: TObject);
    procedure setearFuente;
    procedure recargarFuente;
    procedure GenerarIniFile;
    procedure Timer1Timer(Sender: TObject);
    procedure impCadenaAnchoFijo(cadena: string; ancho:integer);
    function AnsiToOemOp(cadena:string):string;
    procedure ImprimirCadenaRaw(S:String);
    procedure ImprimirCodigoEsc(S:String);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;
  gFileName,gTitle:String;
  gStatusText:TStringList;
  gFuente:TFont;
  gPrintersChars,gIdStatusText:integer;


implementation

{$R *.lfm}

{ TfrmPrincipal }

procedure TfrmPrincipal.FormCreate(Sender: TObject);
begin
     if frmPrincipal.WindowState<>wsMaximized then
        frmPrincipal.WindowState:=wsMaximized;

     gFuente:=TFont.Create;

     gTitle:='RawPad - Radio Belgrano Suardi';
     frmPrincipal.Caption:=gTitle;

     gStatusText:=TStringList.Create;
     gStatusText.Append('www.perseux.com - Crowdsourcing');
     gStatusText.Append('www.perseux.com - Desarrollos de Software multiplataforma a medida con tecnologías Open Source.');
     gStatusText.Append('www.perseux.com - Desarrollo de Sitios Web estáticos y dinámicos a medida y personalizados.');
     gStatusText.Append('www.perseux.com - Asesoramiento para la instalación de redes Wireless, Wired y Fibra Óptica.');
     gStatusText.Append('www.perseux.com - Asesoramiento para la instalación y configuración de Sistemas Servidores.');
     gStatusText.Append('www.perseux.com - Contacto Argentina: +54(3562)514856 | info@perseux.com | rafael.ferrero@perseux.com');
     gStatusText.Append('www.perseux.com - Crowdsourcing');
     gStatusText.Append('RawPad desarrollado con Lazarus/FreePascal por Perseux - info@perseux.com');

     CargarIniFile;
     recargarFuente;

     StatusBar1.SimpleText:=gStatusText.Strings[0];
     Timer1.Interval:=5000;

     if Trim(ParamStr(1))<>'' then begin
        gFileName:=Trim(ParamStr(1));
        if not M_Noticia.Visible then M_Noticia.Visible:=True;
        M_Noticia.Lines.LoadFromFile(gFileName);
     end;

end;

procedure TfrmPrincipal.MI_AbrirClick(Sender: TObject);
begin
     if not M_Noticia.Visible then M_Noticia.Visible:=True;
     M_Noticia.Lines.Clear;

     OpenDialog1.Title:='Indique el archivo a abrir.';
     if OpenDialog1.Execute then begin
        M_Noticia.Lines.LoadFromFile(OpenDialog1.FileName);
        gFileName:=OpenDialog1.FileName;
        frmPrincipal.Caption:=gTitle+' - '+ExtractFileName(gFileName);
     end;
end;

procedure TfrmPrincipal.MI_GuardarClick(Sender: TObject);
var msg:string; rta:integer;
begin
     if gFileName<>'' then begin
        msg:='¿Desea guardar los cambios al archivo '+ExtractFileNameOnly(gFileName)+'?';
        rta:=MessageDlg(msg,mtConfirmation, mbOKCancel, 0);
        if rta=mrOK then begin
           M_Noticia.Lines.SaveToFile(gFileName);
           ShowMessage('Archivo guardado correctamente.');
           frmPrincipal.Caption:=gTitle+' - '+ExtractFileName(gFileName);
        end;
     end
     else MI_GuardarComoClick(Sender);
end;

procedure TfrmPrincipal.MI_GuardarComoClick(Sender: TObject);
var msg:string; rta:integer;
begin
     SaveDialog1.Title:='Indique donde guardar el archivo.';
     if gFileName<>'' then SaveDialog1.InitialDir:=ExtractFilePath(gFileName);

     if SaveDialog1.Execute then begin
        if FileExists(SaveDialog1.FileName) then begin
           msg:='¿Desea sobre escribir el archivo '+ExtractFileNameOnly(SaveDialog1.FileName)+'?';
           rta:=MessageDlg(msg,mtConfirmation, mbOKCancel, 0);
           if rta=mrCancel then exit;
        end;
        gFileName:=SaveDialog1.FileName;
        M_Noticia.Lines.SaveToFile(ExtractFileNameWithoutExt(gFileName)+'.txt');
        ShowMessage('Archivo guardado correctamente.');
        frmPrincipal.Caption:=gTitle+' - '+ExtractFileName(gFileName);
     end;
end;

procedure TfrmPrincipal.MI_ImprimirClick(Sender: TObject);
var i:integer;
begin
     try
          printDialog1.title:='Seleccione impresora para imprimir la noticia.';//En linux con el nuevo parche 2013 se ve.
          if PrintDialog1.Execute then begin
             Printer.RawMode:=True;
             Printer.BeginDoc;
             ImprimirCadenaRaw(chr(27)+CHR(64));
             ImprimirCadenaRaw(chr(18)); //cancelar modo condensado
             ImprimirCadenaRaw(CHR(27)+CHR(67)+CHR(48)); //pagina de 48 lineas
             for i:=0 to M_Noticia.Lines.Count -1 do begin
                 impCadenaAnchoFijo(M_Noticia.Lines[i],gPrintersChars);
             end;
             ImprimirCadenaRaw('');
             {FIN IMPRESION DE NOTICIAS}
             Printer.EndDoc;
             Printer.RawMode:=false;
          end;
     except
       ShowMessage('Probablemente no tenga una impresora instalada.');
     end;
end;

procedure TfrmPrincipal.MI_NuevoClick(Sender: TObject);
begin
     if not M_Noticia.Visible then M_Noticia.Visible:=True;

     M_Noticia.Lines.Clear;

     gFileName:='';
     frmPrincipal.Caption:=gTitle+' - (Archivo sin guardar)';
end;

procedure TfrmPrincipal.M_NoticiaChange(Sender: TObject);
begin
     if gFileName<>'' then
        frmPrincipal.Caption:=gTitle+' - *'+ExtractFileName(gFileName);
end;

function TfrmPrincipal.StrToStyle(cadena: String): TFontStyles;
var estilo:TFontStyles;
begin
     estilo:=[];
     if( Pos( 'fsBold',cadena ) > 0 ) then estilo := estilo + [ fsBold ];
     if( Pos( 'fsItalic',cadena ) > 0 ) then estilo := estilo + [ fsItalic ];
     if( Pos( 'fsUnderline',cadena ) > 0 ) then estilo := estilo + [ fsUnderline ];
     if( Pos( 'fsStrikeout',cadena ) > 0 ) then estilo := estilo + [ fsStrikeout ];
     Result:=estilo;
end;

function TfrmPrincipal.StyleToStr(fuente: TFont): string;
var sStyle:string;
begin
     sStyle:='[';
     with fuente do begin
          if( fsBold in Style )then  sStyle := sStyle + 'fsBold, ';
          if( fsItalic in Style )then sStyle := sStyle + 'fsItalic, ';
          if( fsUnderline in Style )then sStyle := sStyle + 'fsUnderline, ';
          if( fsStrikeout in Style )then sStyle := sStyle + 'fsStrikeout, ';
          if sStyle<>'[' then sStyle:=LeftStr(sStyle, Length(sStyle)-2)+']'
          else sStyle:='[]';
     end;
     Result:=sStyle;
end;

procedure TfrmPrincipal.CargarIniFile;
var archivo:string; ini:TIniFile;
begin
     archivo:=ExtractFilePath(Application.EXEName)+'settings.ini';
     if not FileExists(archivo) then begin
        GenerarIniFile;
        archivo:=ExtractFilePath(Application.EXEName)+'settings.ini';
     end;
     ini:=TIniFile.Create(archivo);
     with ini do begin
          gFuente.Name:=trim(ReadString('Configuraciones', 'Nombre','default'));
          gFuente.Size:=StrToInt(trim(ReadString('Configuraciones', 'Tamano','10')));
          gFuente.Color:=StringToColor(trim(ReadString('Configuraciones', 'Color','clDefault')));
          gFuente.Style:=StrToStyle(trim(ReadString('Configuraciones', 'Estilo','[fsBold]')));
          gPrintersChars:=StrToInt(trim(ReadString('Configuraciones', 'CantCharPrinter','70')));
     end;
end;

procedure TfrmPrincipal.GenerarIniFile;
var iniVacio:TStringList; msg:string;
begin
     msg:='ERROR!!!'+#13+'No se encuentra el archivo settings.ini, el mismo será creado.';
     ShowMessage(msg);

     buscarFuente;

     iniVacio:= TStringList.Create;
     iniVacio.Clear;
     iniVacio.Add('[Configuraciones]');
     iniVacio.Add('Nombre='+gFuente.Name);
     iniVacio.Add('Tamano='+IntToStr(gFuente.Size));
     iniVacio.Add('Color='+ColorToString(gFuente.Color));
     iniVacio.Add('Estilo='+StyleToStr(gFuente));
     iniVacio.Add('CantCharPrinter=70');
     iniVacio.SaveToFile(ExtractFilePath(Application.EXEName)+'settings.ini');
end;

procedure TfrmPrincipal.Timer1Timer(Sender: TObject);
begin
     if gIdStatusText>gStatusText.Count-1 then gIdStatusText:=0;
     StatusBar1.SimpleText:=gStatusText.Strings[gIdStatusText];
     gIdStatusText:=gIdStatusText+1;
end;

procedure TfrmPrincipal.buscarFuente;
var msg:string;
begin
     msg:='Configure la fuente que desea usar en el programa'+#13;
     msg:=msg+'(La fuente indicada y su tamaño no influye en la impresión.)';
     ShowMessage(msg);
     if FontDialog1.Execute then begin
        gFuente.Assign(FontDialog1.Font);
     end;
end;

procedure TfrmPrincipal.MI_ConfiguracionClick(Sender: TObject);
begin
    buscarFuente;
    setearFuente;
    recargarFuente;
end;

procedure TfrmPrincipal.setearFuente;
var archivo:string; ini:TIniFile;
begin
    archivo:=ExtractFilePath(Application.EXEName)+'settings.ini';
    if not FileExists(archivo) then begin
       GenerarIniFile;
       archivo:=ExtractFilePath(Application.EXEName)+'settings.ini';
    end;
    ini := TIniFile.Create(archivo);
    with ini do begin
        WriteString('Configuraciones', 'Nombre',gFuente.Name );
	WriteString('Configuraciones', 'Tamano',IntToStr(gFuente.Size) );
	WriteString('Configuraciones', 'Color',ColorToString(gFuente.Color) );
	WriteString('Configuraciones', 'Estilo',StyleToStr(gFuente) );
    end;
end;

procedure TfrmPrincipal.recargarFuente;
var  i:integer; componente:tcomponent;
begin
     with frmPrincipal do begin
        for i:=0 to ComponentCount-1 do begin
            componente := Components[i];
            if (componente is TMemo) then TMemo(componente).Font:=gFuente;
        end;
     end;
end;

procedure TfrmPrincipal.impCadenaAnchoFijo(cadena: string; ancho:integer);
var i:integer;
begin
  if Length(cadena)>ancho then begin
     if CompareStr(' ',Copy(cadena,ancho-1,1))<>0 then begin
        for i:=ancho downto 0 do begin
            if CompareStr(' ',Copy(cadena,i-1,1))=0 then begin
               ImprimirCadenaRaw(Trim(Copy(cadena,0,i-1)));
               impCadenaAnchoFijo(Trim(Copy(cadena,i,Length(cadena))),ancho);
               exit;
            end;
        end;
     end
     else begin
          ImprimirCadenaRaw(Trim(Copy(cadena,0,ancho-1)));
          impCadenaAnchoFijo(Trim(Copy(cadena,ancho,Length(cadena))),ancho);
     end;
  end
  else begin
      ImprimirCadenaRaw(cadena);
  end;
end;

//tiene los valores en español
function TfrmPrincipal.AnsiToOemOp(cadena:string):string;
var tabla:array[128..255]of char;caracter:char;
    CadenaOem:string; i:integer;
begin
    tabla[199]:=#128;    tabla[252]:=#129;
    tabla[233]:=#130;    tabla[226]:=#131;
    tabla[228]:=#132;    tabla[224]:=#133;
    tabla[229]:=#134;    tabla[231]:=#135;
    tabla[234]:=#136;    tabla[235]:=#137;
    tabla[232]:=#138;    tabla[239]:=#139;
    tabla[238]:=#140;    tabla[236]:=#141;
    tabla[196]:=#142;    tabla[197]:=#143;
    tabla[201]:=#144;    tabla[230]:=#145;
    tabla[198]:=#146;    tabla[244]:=#147;
    tabla[246]:=#148;    tabla[242]:=#149;
    tabla[251]:=#150;    tabla[249]:=#151;
    tabla[255]:=#152;    tabla[214]:=#153;
    tabla[220]:=#154;    tabla[248]:=#155;
    tabla[163]:=#156;    tabla[216]:=#157;
    tabla[215]:=#158;    tabla[131]:=#159;
    tabla[225]:=#160;    tabla[237]:=#161;
    tabla[243]:=#162;    tabla[250]:=#163;
    tabla[241]:=#164;    tabla[209]:=#165;
    tabla[170]:=#166;    tabla[186]:=#167;
    tabla[191]:=#168;    tabla[174]:=#169;
    tabla[172]:=#170;    tabla[189]:=#171;
    tabla[188]:=#160;    tabla[161]:=#161;
    tabla[171]:=#174;    tabla[187]:=#175;
    tabla[166]:=#176;    tabla[166]:=#177;
    tabla[166]:=#178;    tabla[166]:=#179;
    tabla[166]:=#180;    tabla[193]:=#181;
    tabla[194]:=#182;    tabla[192]:=#183;
    tabla[169]:=#184;    tabla[166]:=#185;
    tabla[166]:=#186;
    //tabla[43]:=#187;
    //tabla[43]:=#188;
     tabla[162]:=#189;
    tabla[165]:=#190;   // tabla[43]:=#191;
    //tabla[43]:=#192;     tabla[45]:=#193;
  //  tabla[45]:=#194;     tabla[43]:=#195;
//    tabla[45]:=#196;     tabla[43]:=#197;
    tabla[227]:=#198;    tabla[195]:=#199;
//    tabla[43]:=#200;     tabla[43]:=#201;
  //  tabla[45]:=#202;     tabla[45]:=#203;
    tabla[166]:=#204;   // tabla[45]:=#205;
    //tabla[43]:=#206;
    tabla[164]:=#207;
    tabla[240]:=#208;    tabla[208]:=#209;
    tabla[202]:=#210;    tabla[203]:=#211;
    tabla[200]:=#212;    //tabla[105]:=#213;
    tabla[205]:=#214;    tabla[206]:=#215;
    tabla[207]:=#216;
//    tabla[43]:=#217;    tabla[43]:=#218;
    tabla[166]:=#219;
    //tabla[95]:=#220;
    tabla[166]:=#221;
    tabla[204]:=#222;    tabla[175]:=#223;
    tabla[211]:=#224;    tabla[223]:=#225;
    tabla[212]:=#226;    tabla[210]:=#227;
    tabla[245]:=#228;    tabla[213]:=#229;
    tabla[181]:=#230;    tabla[254]:=#231;
    tabla[222]:=#232;    tabla[218]:=#233;
    tabla[219]:=#234;    tabla[217]:=#235;
    tabla[253]:=#236;    tabla[221]:=#237;
    tabla[175]:=#238;    tabla[180]:=#239;
    tabla[173]:=#240;    tabla[177]:=#241;
    //tabla[61]:=#242;
    tabla[190]:=#243;
    tabla[182]:=#245;    tabla[167]:=#246;
    tabla[247]:=#246;    tabla[184]:=#247;
    tabla[176]:=#248;    tabla[168]:=#249;
    tabla[183]:=#250;    tabla[185]:=#251;
    tabla[179]:=#252;    tabla[178]:=#253;
    tabla[166]:=#254;    tabla[160]:=#255;
//--
    CadenaOem:='';
    for i := 1 to Length(cadena) do begin
        caracter:=cadena[i];
        if ord(caracter)>=128 then begin
            Caracter:=tabla[ord(caracter)];
        end;
        CadenaOem:=CadenaOem+ Caracter;
    end;
    result:=CadenaOem;
end;

procedure TfrmPrincipal.ImprimirCadenaRaw(S:String);
const Kenter=CHR(13)+CHR(10);
begin
    {$ifdef windows}
    S:=Utf8ToAnsi(S+Kenter);
    S:=AnsiToOemOp(s);//porque las matriciales esperan cadenas OEM
    {$else}
    S:=UTF8ToCP850(S+Kenter);
    {$endif}

    ImprimirCodigoEsc(S);
end;

procedure TfrmPrincipal.ImprimirCodigoEsc(S:String);
var Written: Integer;
begin
    Written:=0;
    Printer.Write(S[1], Length(S), Written);
end;

end.
